import numpy as np
import pandas as pd

df_trend_all = pd.read_csv('data/SearchTrend.csv')
df_trend = df_trend_all.loc[df_trend_all.date > "2019"]

dates = sorted(df_trend.date.unique())[-367:]
trends = ["cold", "flu", "pneumonia", "coronavirus"]

# make figure
fig_dict = {
    "data": [],
    "layout": {
        'font': {'color': '#7f7f7f', 'size': 18},
        'plot_bgcolor': 'white',
        'title': {'text': f'Search Trend in South Korea {min(dates)} - {max(dates)}'},
        'height': 650
    },
    "frames": []
}

# fill in most of layout
fig_dict["layout"]["xaxis"] = {"range": [min(dates), max(dates)], "title": "Date"}
fig_dict["layout"]["yaxis"] = {"range": [-3, 2], "title": "The search volume in Korean language [log]", "type": "log"}
fig_dict["layout"]["hovermode"] = "closest"
fig_dict["layout"]["updatemenus"] = [
    {
        "buttons": [
            {
                "args": [None, {"frame": {"duration": 200, "redraw": False},
                                "fromcurrent": True, "transition": {"duration": 30,
                                                                    "easing": "quadratic-in-out"}}],
                "label": "Play",
                "method": "animate"
            },
            {
                "args": [[None], {"frame": {"duration": 0, "redraw": False},
                                  "mode": "immediate",
                                  "transition": {"duration": 0}}],
                "label": "Pause",
                "method": "animate"
            }
        ],
        "direction": "left",
        "pad": {"r": 10, "t": 87},
        "showactive": False,
        "type": "buttons",
        "x": 0.1,
        "xanchor": "right",
        "y": 0,
        "yanchor": "top"
    }
]

sliders_dict = {
    "active": 0,
    "yanchor": "top",
    "xanchor": "left",
    "currentvalue": {
        "font": {"size": 20},
        "prefix": "Day: ",
        "visible": True,
        "xanchor": "right"
    },
    "transition": {"duration": 30, "easing": "cubic-in-out"},
    "pad": {"b": 10, "t": 50},
    "len": 0.9,
    "x": 0.1,
    "y": 0,
    "steps": []
}

# make data
date = dates[0]
for trend in trends:
    df = df_trend.loc[df_trend.date <= date]
    x = df.date.values.tolist()
    y = df[trend].values.tolist()
    
    data_dict = {
        "x": x,
        "y": y,
        "name": trend,
    }
    fig_dict["data"].append(data_dict)

# make frames
for date in dates:
    frame = {"data": [], "name": date}
    df = df_trend.loc[df_trend.date <= date]
    for trend in trends:
        x = df.date.tolist()
        y = df[trend].tolist()
        data_dict = {
            "x": x,
            "y": y,
            "name": trend,
        }
        frame["data"].append(data_dict)

    fig_dict["frames"].append(frame)
    slider_step = {"args": [
        [date],
        {"frame": {"duration": 200, "redraw": False},
         "mode": "immediate",
         "transition": {"duration": 30}}
    ],
        "label": date,
        "method": "animate"}
    sliders_dict["steps"].append(slider_step)


fig_dict["layout"]["sliders"] = [sliders_dict]
fig_dict["layout"]['showlegend'] = True

trend_figure = fig_dict

dates = df_trend_all.date.values

# make figure
fig_dict = {
    "data": [],
    "layout": {
        'font': {'color': '#7f7f7f', 'size': 18},
        'plot_bgcolor': 'white',
        'title': {'text': f'Search Trend in South Korea {min(dates)} - {max(dates)}'},
        'height': 600,
    },
    "frames": []
}

# fill in most of layout
fig_dict["layout"]["xaxis"] = {"range": [min(dates), max(dates)], "title": "Date"}
fig_dict["layout"]["yaxis"] = {"range": [-3, 2], "title": "The search volume in Korean language [log]", "type": "log"}
fig_dict["layout"]["hovermode"] = "closest"
fig_dict["layout"]["updatemenus"] = [
    {
        "direction": "left",
        "pad": {"r": 10, "t": 87},
        "showactive": False,
        "type": "buttons",
        "x": 0.1,
        "xanchor": "right",
        "y": 0,
        "yanchor": "top"
    }
]

for trend in trends:
    x = df_trend_all.date.values.tolist()
    y = df_trend_all[trend].values.tolist()
    
    data_dict = {
        "x": x,
        "y": y,
        "name": trend,
    }
    fig_dict["data"].append(data_dict)
    
fig_dict["layout"]['showlegend'] = True

history_figure = fig_dict


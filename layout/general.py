import pandas as pd
import numpy as np

data = pd.read_csv('data/general_data.csv', index_col=0, parse_dates=True)
percent_data = pd.read_csv('data/percent_data.csv', index_col=0, parse_dates=True)

range_selector = dict(
    buttons=list([
        dict(count=1 * 7, label="1w", step="day", stepmode="backward"),
        dict(count=2 * 7, label="2w", step="day", stepmode="backward"),
        dict(count=3 * 7, label="3w", step="day", stepmode="backward"),
        dict(count=1, label="1m", step="month", stepmode="backward"),
        dict(count=2, label="2m", step="month", stepmode="backward"),
        dict(step="all")
    ]))

cases_figure = {
    'data': [{
        'x': data.index,
        'y': data.total_confirmed,
        'textfont': {'size': 24},
        'type': 'scatter',
        'hovertemplate': '<b>Date</b>: %{x}<br><b>Cases</b>: %{y}<br><extra></extra>'
    }],
    'layout': {
        'font': {'color': '#7f7f7f', 'size': 18},
        'height': 650,
        'title': {'text': 'Total confirmed cases'},
        'yaxis': {'title': {'text': 'Cases'}},
        'xaxis': {'title': {'text': 'Date'},
                  'rangeslider': {'visible': True},
                  'rangeselector': range_selector
                  }
    }
}

log_cases_figure = {
    'data': [{
        'x': data.index,
        'y': np.log(data.total_confirmed),
        'textfont': {'size': 24},
        'type': 'scatter',
        'hovertemplate': '<b>Date</b>: %{x}<br><b>Cases</b>: %{text}<br><extra></extra>',
        'text': data.total_confirmed
    }],
    'layout': {
        'font': {'color': '#7f7f7f', 'size': 18},
        'height': 650,
        'title': {'text': 'Logarithm of total confirmed cases'},
        'yaxis': {'title': {'text': 'ln(Cases)'}},
        'xaxis': {'title': {'text': 'Date'},
                  'rangeslider': {'visible': True},
                  'rangeselector': range_selector}
    }
}

daily_cases_figure = {
    'data': [{
        'x': data.index,
        'y': data.new_confirmed,
        'textfont': {'size': 24},
        'hovertemplate': '<b>Date</b>: %{x}<br><b>New cases</b>: %{y}<extra></extra>',
        'type': 'bar'
    }],
    'layout': {
        'font': {'color': '#7f7f7f', 'size': 18},
        'height': 650,
        'title': {'text': 'Daily new cases'},
        'yaxis': {'title': {'text': 'Cases'}},
        'xaxis': {'title': {'text': 'Date'},
                  'rangeslider': {'visible': True},
                  'rangeselector': range_selector},
        'bargap': 0.1
    }
}

active_figure = {
    'data': [{
        'x': data.index,
        'y': data.active,
        'textfont': {'size': 24},
        'type': 'scatter',
        'mode': 'lines',
        'hovertemplate': '<b>Date</b>: %{x}<br><b>Active cases</b>: %{y}<extra></extra>',
        'text': data.new_tests
    }],
    'layout': {
        'font': {'color': '#7f7f7f', 'size': 18},
        'title': {'text': 'Active cases'},
        'yaxis': {'title': {'text': 'Number of infected people'}},
        'xaxis': {'title': {'text': 'Date'}}
    }
}

tests_figure = {
    'data': [{
        'x': data.index,
        'y': data.new_tests_per_thousand,
        'textfont': {'size': 24},
        'type': 'scatter',
        'mode': 'lines+markers',
        'hovertemplate': '<b>Date</b>: %{x}<br><b>Tests per thousand</b>: %{y:.2f}<br><b>Number of tests</b>: %{text}<extra></extra>',
        'text': data.new_tests
    }],
    'layout': {
        'font': {'color': '#7f7f7f', 'size': 18},
        'title': {'text': 'Daily performed tests per thousand'},
        'yaxis': {'title': {'text': 'Number of tests per thousand'}},
        'xaxis': {'title': {'text': 'Date'}}
    }
}

death_rate_figure = {
    'data': [
        {'x': data.index,
         'y': data.death_rate_total,
         'textfont': {'size': 24},
         'type': 'scatter',
         'mode': 'lines',
         'hovertemplate': '<b>Date</b>: %{x}<br><b>Rate</b>: %{y}',
         'name': 'Death rate by total cases'},
        {'x': data.index,
         'y': data.death_rate_ended,
         'textfont': {'size': 24},
         'type': 'scatter',
         'mode': 'lines',
         'hovertemplate': '<b>Date</b>: %{x}<br><b>Rate</b>: %{y}',
         'name': 'Death rate by ended cases'
         }],
    'layout': {
        'font': {'color': '#7f7f7f', 'size': 18},
        'title': {'text': 'Death rate'},
        'hovermode': "x",
        'yaxis': {'title': {'text': 'Death rate'}},
        'xaxis': {'title': {'text': 'Date'},
                  'rangeslider': {'visible': True},
                  'rangeselector': range_selector},
        'height': 650,
        'legend': {'orientation': "h",
                   'x': 0.45,
                   'y': 1.0}
    }
}

daily_deaths_figure = {
    'data': [{
        'x': data.index,
        'y': data.new_deaths,
        'textfont': {'size': 24},
        'hovertemplate': '<b>Date</b>: %{x}<br><b>Deaths</b>: %{y}<extra></extra>',
        'type': 'bar'
    }],
    'layout': {
        'font': {'color': '#7f7f7f', 'size': 18},
        'title': {'text': 'Daily new deaths'},
        'yaxis': {'title': {'text': 'Number of deaths'}},
        'bargap': 0.1,
        'xaxis': {'title': {'text': 'Date'},
                  'rangeslider': {'visible': True},
                  'rangeselector': range_selector},
    }}

percent_figure_data = [
    {'x': percent_data.index,
     'y': percent_data[col],
     'textfont': {'size': 24},
     'type': 'scatter',
     'mode': 'lines',
     'line': {'width': 4 if col == 'South Korea' else 2},
     'hovertemplate': '<b>Date</b>: %{x}<br><b>Contribution</b>: %{y:.2f}%',
     'name': col} for col in percent_data.columns]

percent_figure = {
    'data': percent_figure_data,
    'layout': {
        'font': {'color': '#7f7f7f', 'size': 18},
        'title': {'text': 'Contribution to the world'},
        'hovermode': "x",
        'yaxis': {'title': {'text': '%'}},
        'xaxis': {'title': {'text': 'Date'},
                  'rangeslider': {'visible': True},
                  'rangeselector': range_selector},
        'height': 650,
    }
}

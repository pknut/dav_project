import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import dash_table

from .general import *
from .patients import (
    df_patient,
    sex_distribution_figure,
    age_group_distribution_figure,
    nationality_figure,
    contact_number_figure)
from .searchtrend import trend_figure, history_figure

"""-------------------------------INDEX--------------------------------------"""


def index_info_card(header, fa_icon, info):
    return dbc.Card([
        dbc.CardHeader(
            dbc.Row([
                dbc.Col(html.I(className=f"fas {fa_icon} fa-2x"), className="col-md-3 my-auto"),
                html.P(header, className="col-md-9 my-auto")
            ])
        ),
        dbc.CardBody([html.P(info, className='card-text')]),
    ])


def logo_col(logo_href, img_src, img_class, title, text=''):
    return dbc.Col([
        html.A(
            href=logo_href,
            children=html.Div(
                html.Img(className=img_class, src=img_src),
                className="tools-logo circle-img"
            )
        ),
        html.Div([html.H2(title), html.P(text)]),
    ], className='tools-logo')


def index():
    tools_logos = dbc.Row([
        logo_col(
            logo_href="/general",
            img_src="https://image.flaticon.com/icons/svg/2920/2920253.svg",
            img_class="small_icon",
            title="General",
            # text="General analysis ..."
        ),
        logo_col(
            logo_href="/patients",
            img_src="https://image.flaticon.com/icons/svg/2302/2302763.svg",
            img_class="small_icon",
            title="Patients",
            # text="""In South Korea people who were confirmed to be infected in a hospital, 
            # completed a survey. All these data were anonymized and made public by KCDC 
            # (Korea Centers for Disease Control & Prevention)."""
        ),
        logo_col(
            logo_href="/search",
            img_src="https://image.flaticon.com/icons/svg/2913/2913158.svg",
            img_class="small_icon",
            title="Search Trend",
            # text="Flights analysis ...",
        ),
    ])

    return html.Div([
        html.H2("South Korea: covid analysis"),
        tools_logos,
    ], className="container")


"""------------------------------GENERAL-------------------------------------"""

cases_graphs = html.Div(dcc.Tabs([
    dcc.Tab(label='Linear', children=[
        dcc.Graph(
            id='cases',
            figure=cases_figure)
    ]),
    dcc.Tab(label='Logarithmic', children=[
        dcc.Graph(
            id='log_cases',
            figure=log_cases_figure)
    ]),
    dcc.Tab(label='Daily', children=[
        dcc.Graph(
            id='daily_cases',
            figure=daily_cases_figure)
    ]),
]),
    className="graph")

active_graph = html.Div(
    dcc.Graph(
        id='active',
        figure=active_figure
    ),
    className='graph'
)

tests_graph = html.Div(
    dcc.Graph(
        id='tests',
        figure=tests_figure
    ),
    className='graph'
)

death_graphs = html.Div([dcc.Graph(
    id='tests',
    figure=death_rate_figure),
    dcc.Graph(
        id='log_cases',
        figure=daily_deaths_figure)],
    className="graph")

percent_graph = html.Div(
    dcc.Graph(
        id='percent',
        figure=percent_figure),
    className="graph")


def general():
    return html.Div([
        html.H2('General statistics for coronavirus data in South Korea'),
        html.Br(),
        html.Br(),
        html.H3('Confirmed cases'),
        cases_graphs,
        html.Br(),
        html.Br(),
        html.H3('Currently coronavirus infected people'),
        active_graph,
        html.Br(),
        html.Br(),
        html.H3('Performed tests'),
        tests_graph,
        html.Br(),
        html.Br(),
        html.H3('Deaths analysis'),
        death_graphs,
        html.Br(),
        html.Br(),
        html.H3('Contribution in cases around the world'),
        percent_graph,

    ])


"""------------------------------PATIENTS------------------------------------"""


def patients():
    patients_profiles = html.Div([
        dbc.Row([
            html.Div(
                html.Img(
                    className='small_icon',
                    src="https://image.flaticon.com/icons/svg/2750/2750657.svg"
                ),
                className="tools-logo circle-img"
            ),
            dbc.Col([
                html.H4("Patient ID: 1100000001"),
                html.H4("Sex: Male"),
                html.H4("Birth year: 2001"),
                html.H4("Country: Korea"),
                html.H4("Infection case: Onchun Church"),
                html.H4("Contact number: 1160"),
                html.H4("Confirmed_date: 2020-02-21")
            ]),
            html.Div(
                html.Img(
                    className='small_icon',
                    src="https://image.flaticon.com/icons/svg/2750/2750667.svg"
                ),
                className="tools-logo circle-img"
            ),
            dbc.Col([
                html.H4("Patient ID: 1200000031"),
                html.H4("Sex: Female"),
                html.H4("Birth year: 1959"),
                html.H4("Country: Korea"),
                html.H4("Infection case: Shincheonji Church"),
                html.H4("Contact number: 1091"),
                html.H4("Confirmed_date: 2020-02-18")
            ])
        ]),
    ], className='patient')

    return html.Div([
        html.H2('South Korea: Patient Analysis'),
        html.Div([
            """
            In South Korea people who were confirmed to be infected in a hospital, completed a survey. 
            All these data were anonymized and made public by KCDC (Korea Centers for Disease 
            Control & Prevention).
            """,
            html.Br(),
            html.Br(),
            # dash_table.DataTable(
            #     id='patient_table',
            #     columns=[{"name": i, "id": i} for i in df_patient.columns],
            #     data=df_patient.to_dict('records'),
            #     page_action="native",
            #     page_current=0,
            #     page_size=10
            # )
            ], className="table"),
        html.Div(dcc.Graph(
            id='sex_distribution',
            figure=sex_distribution_figure
        ), className="graph"),
        html.Div(dcc.Graph(
            id='age_distribution',
            figure=age_group_distribution_figure
        ), className="graph"),
        html.Div(dcc.Graph(
            id='nationality_distribution',
            figure=nationality_figure
        ), className="graph"),
        html.Div(dcc.Graph(
            id='contact_number',
            figure=contact_number_figure
        ), className="graph"),
        patients_profiles
    ])


"""------------------------------FLIGHTS-------------------------------------"""


def flights():
    return html.Div([
        html.H2("Search Trend"),
        html.H4("""Trend data of the keywords searched in NAVER which is one of the 
        largest portals in South Korea"""),
        html.Br(),
        html.Div(dcc.Graph(
            id='trend',
            figure=trend_figure
        ), className="graph"),
        html.Br(),
        html.H2("What is the impact of the natural cycle associated with the seasons?"),
        html.Div(dcc.Graph(
            id='trend',
            figure=history_figure
        ), className="graph"),
        html.Br()
    ])

import pandas as pd


df_patient = pd.read_csv('data/patients.csv')

sex_distribution_figure={
    'data': [{
        'hoverinfo': 'label+percent',
        'labels': ['female', 'male'],
        'marker': {'colors': ['#FFC0CB', '#6495ED'], 'line': {'color': '#FFFFFF', 'width': 2}},
        'textfont': {'size': 24},
        'textinfo': 'value',
        'type': 'pie',
        'values': [1856, 1455]
    }],
    'layout': {
        'font': {'color': '#7f7f7f', 'size': 18}, 
        'title': {'text': 'Distribution of sex'},
        # 'paper_bgcolor': 'rgba(255,255,255,0.9)',
    }
}

age_group_distribution_figure = {
    'data': [{
        'marker': {'color': '#FFC0CB'},
        'name': 'female',
        'opacity': 0.9,
        'type': 'bar',
        'x': [  0.,  10.,  20.,  30.,  40.,  50.,  60.,  70.,  80.,  90., 100.],
        'y': [ 10,  24, 248, 239, 227, 303, 297, 120, 115,  65,  10]
    },
    {
        'marker': {'color': '#6495ED'},
        'name': 'male',
        'opacity': 0.9,
        'type': 'bar',
        'x': [  0.,  10.,  20.,  30.,  40.,  50.,  60.,  70.,  80.,  90., 100.],
        'y': [ 13,  39, 230, 227, 189, 151, 222, 101,  65,  29,   2]
    }],
    'layout': {
        'bargap': 0.2,
        'bargroupgap': 0.1,
        'font': {'color': '#7f7f7f', 'size': 18},
        'plot_bgcolor': 'white',
        'template': '...',
        'title': {'text': 'Patients age distribution'},
        'xaxis': {'title': {'text': 'Age group'}},
        'yaxis': {'title': {'text': 'Count'}}
    }
}

nationality_figure = {
    'data': [{
        'marker': {'color': '#FFC0CB'},
        'name': 'female',
        'opacity': 0.9,
        'type': 'bar',
        'x': ['Canada', 'China', 'France', 'Korea', 'Thailand', 'United States', 'Vietnam'],
        'y': [   1,    7,    1, 1843,    1,    2,    1]
    },
    {
        'marker': {'color': '#6495ED'},
        'name': 'male',
        'opacity': 0.9,
        'type': 'bar',
        'x': ['China', 'Germany', 'Indonesia', 'Korea', 'Mongolia', 'Spain', 'Switzerland', 'Thailand', 'United States'],
        'y': [   4,    1,    1, 1441,    1,    1,    1,    1,    4]
    }],
    'layout': {
        'bargap': 0.2,
        'bargroupgap': 0.1,
        'font': {'color': '#7f7f7f', 'size': 18},
        'plot_bgcolor': 'white',
        'margin': {'b': 120},
        'template': '...',
        'height': 500,
        'title': {'text': 'Patient nationality distribution'},
        'xaxis': {'title': {'text': 'Nationality of the patient'}},
        'yaxis': {'ticksuffix': '   ', 'title': {'text': 'Count [log]'}, 'type': 'log'}}
}

contact_number_figure = {
    'data': [{
        'marker': {'color': '#FFC0CB'},
        'name': 'female',
        'opacity': 0.9,
        'type': 'box',
        'x': df_patient.loc[df_patient.sex == 'female'].contact_number,
    },
    {
        'marker': {'color': '#6495ED'},
        'name': 'male',
        'opacity': 0.9,
        'type': 'box',
        'x': df_patient.loc[df_patient.sex == 'male'].contact_number,
    }],
    'layout': {
        'bargap': 0.2,
        'bargroupgap': 0.1,
        'font': {'color': '#7f7f7f', 'size': 18},
        'plot_bgcolor': 'white',
        'template': '...',
        'title': {'text': 'Distribution of contact number'},
        'xaxis': {'title': {'text': 'Number of contacts'}}}
}
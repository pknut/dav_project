# DAV Project: South Korea vs. covid-19


## Installation (linux)

1) First you need to clone this repository:
```bash
$ git clone git@gitlab.com:pknut/dav_project.git
# or
$ git clone https://gitlab.com/pknut/dav_project.git
```

2) Create new virtualenv with python>=3.6:
```bash
$  virtualenv -p python3.6 your_venv_name
```

3) Activate your virtualenv:
```bash
$ source your_venv_name/bin/activate
```
The name of the current virtual environment appears to the left of the prompt. To exit your virtualenv just type `deactivate`.

4) Change directory:
```
$ cd dav_project
```

5) Install requirements:
```bash
$ pip install -r requirements.txt
```

## Usage

To use application you need to activate your virtualenv (`$ source your_venv_name/bin/activate`) and be in the dav_project folder.

```
$ python app.py
```
A link to the application should appear in the terminal. Open the application by clicking on it or typing `127.0.0.1/8050` in the web browser.

## Data source

[Kaggle dataset](https://www.kaggle.com/kimjihoo/coronavirusdataset?select=PatientInfo.csv)

[Gihub CSSEGISandData](https://github.com/CSSEGISandData/COVID-19?fbclid=IwAR1_HJ9TMdcNSdsUBnlLsIzIW88r1KSb0FIFQ6I3GzbbFVH1SwLcf_D9jWY)

[Our World in Data](https://ourworldindata.org/coronavirus#our-world-in-data-relies-on-data-from-the-world-health-organization)
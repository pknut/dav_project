import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from layout import pages

external_css = [
    'https://use.fontawesome.com/releases/v5.8.1/css/all.css'
]
app = dash.Dash(__name__, external_stylesheets=external_css)
app.title = 'DAV Project'
server = app.server

def get_nav_link(fa_icon, span, href, id):
    return html.Li(html.A(
        [
            html.I(className=f"fas fas-nav {fa_icon}"), 
            html.Span(span, className="nav-text")
        ],
        href=href, 
        id=id), className="has-subnav high")


app.layout = html.Div([
    dcc.Location(id="url", refresh=False),
    html.Div([], className="area"),
    dbc.Navbar([
        html.Ul([
            get_nav_link("fa-home", "Home", "/#", "index_nav"),
            get_nav_link("fa-chart-line", "General Analysis", "/general", "general_nav"),
            get_nav_link("fa-procedures", "Patients Analysis", "/patients", "patients_nav"),
            get_nav_link("fa-wifi", "Search Trend Analysis", "/search", "flights_nav"),
        ])
    ], className="main-menu", sticky="left"),
    html.Div(id="page_content", style={'margin-left': '60px'})
])

@app.callback([Output("page_content", 'children'),
               Output("index_nav", 'className'),
               Output("general_nav", 'className'),
               Output("patients_nav", 'className'),
               Output("flights_nav", 'className')],
              [Input("url", 'pathname')])
def display_page(pathname):
    if pathname == '/search':
        return pages.flights(), "", "", "", "active_link"
    elif pathname == '/patients':
        return pages.patients(), "", "", "active_link", ""
    elif pathname == '/general':
        return pages.general(), "", "active_link", "", ""
    else:
        return pages.index(), "active_link", "", "", ""


if __name__ == '__main__':
    app.run_server(debug=True)